﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PubNub
{
    //Given an array with length N, filled with exactly the integer values 0 through (N-2),
    //give me any duplicates in the array
    class Program
    {
        static void Main(string[] args)
        {
            var n = 100;
            var randomizedList = GenerateArray(n).ToList();

            //Solution with LINQ
            var duplicatesList = randomizedList.GroupBy(x => x)
                                                .Where(y => y.Count() > 1)
                                                .Select(z => z.Key)
                                                .ToList();

            //Solution with HashSet
            var hashValues = new HashSet<int>();
            var hashDuplicates = new HashSet<int>();

            randomizedList.ForEach(x => {
                if (hashValues.Contains(x)) {
                    hashDuplicates.Add(x);
                } 
                    hashValues.Add(x);
            });

            //Write on console array and solutions
            foreach (var item in randomizedList)
            {
                Console.WriteLine("Random value " + item);
            }
            foreach (var item in duplicatesList)
            {
                Console.WriteLine("Duplicate from list " + item);
            }
            foreach (var item in hashDuplicates)
            {
                Console.WriteLine("Duplicate from hashSet " + item);
            }

            Console.WriteLine("Duplicates length: " + duplicatesList.Count());
            Console.WriteLine("Hash length: " + hashDuplicates.Count());

        }

        private static int[] GenerateArray(int n)
        {
            var random = new Random();
            var maxValue = n - 2;
            var result = new int[n];

            for (int i = 0; i < n; i++)
            {
                result[i] = random.Next(maxValue);
            }

            return result;
        }
    }
}
